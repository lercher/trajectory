package main

import (
	"image"
	"image/png"
	"io"
	"math"

	"github.com/usedbytes/hsv"
)

func makeImage(wr io.Writer, imgWidth float64, steps int, epsilon float64) error {
	rect := image.Rect(0, 0, int(imgWidth), int(imgWidth))
	img := image.NewNRGBA(rect)
	imgwalk(img, imgWidth, steps, epsilon)
	return png.Encode(wr, img)
}

func imgwalk(img *image.NRGBA, imgWidth float64, steps int, epsilon float64) {
	walk00(steps, epsilon, func(i int, x, y float64) {
		// lg := math.Log(math.Abs(v))
		lg360 := math.Trunc(math.Remainder(float64(i)/imgWidth, 360))
		col := hsv.HSVColor{H: uint16(lg360), S: 180, V: 255} // H: uint16(0..359)
		ix, iy := int(x*imgWidth), int(y*imgWidth)
		img.Set(ix, iy, col)
	})
}
