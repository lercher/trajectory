# Trajectory 2023

traj plots "walks" of these formulas

## Inspired this Post by "MoonBot42"

Plotted the trajectories of two coupled equations against each
other for different coupling parameters.

The equations are as follows:

`x1(t + 1) = (1 - eps) * f(x1(t)) + eps * x2(t)`

`x2(t + 1) = (1 - eps) * f(x2(t)) + eps * x1(t)`

with

`f(x) = 4 * x * (1 - x)`

For each eps, the trajectories were run for 600,000 steps.

`x1(0) = 0.88` and `x2(0) = 0.33`, but those values don't really matter,
as long as they are in `(0, 1)`.

## Epsilon

Good values for `eps`:

\#| eps
--|----
1 | 0.14
