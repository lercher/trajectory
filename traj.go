package main

import (
	"log"
	"os"
)

func main() {
	log.Println("This is trajectory, (c) 2023 by Martin Lercher")

	if false {
		printwalk(100, 5e-2)
	} else {
		o, err := os.Create("img.png")
		if err != nil {
			log.Fatalln(err)
		}
		defer o.Close()

		makeImage(o, 1920, 1_500_000, 0.14)
	}
	log.Println("done")
}

func printwalk(steps int, epsilon float64) {
	walk00(steps, epsilon, func(i int, x1, x2 float64) {
		log.Println(i, x1, x2)
	})
}

func walk00(steps int, eps float64, result func(i int, x, y float64)) {
	x, y := 0.88, 0.33
	for i := 0; i < steps; i++ {
		x, y = (1-eps)*f(x)+eps*y, (1-eps)*f(y)+eps*x
		result(i, x, y)
	}
}

func f(a float64) float64 {
	return 4 * a * (1 - a)
}
